﻿using Newtonsoft.Json;
using SocialIntergrationCore.Net.Libraries;

namespace SocialIntergrationCore.Net.Slack
{
    public class SlackField : IField
    {
        /// <summary>
        /// Title of this Field
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }

        /// <summary>
        /// The Value of this Field
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }

        /// <summary>
        /// Is this field Inlined
        /// </summary>
        [JsonProperty("short")]
        public bool Inline { get; set; }
    }
}