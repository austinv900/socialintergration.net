﻿using Newtonsoft.Json;
using SocialIntergrationCore.Net.Libraries;

namespace SocialIntergrationCore.Net.Slack
{
    public class SlackMessage : IMessage
    {
        /// <summary>
        /// Tells this message to go to another channel instead of defult
        /// </summary>
        [JsonProperty("channel")]
        public string Channel { get; set; }

        /// <summary>
        /// The main message to be shown
        /// </summary>
        [JsonProperty("text")]
        public string Message { get; set; }

        /// <summary>
        /// The name the message came from
        /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }

        /// <summary>
        /// The Authors Avatar
        /// </summary>
        [JsonProperty("icon_url")]
        public string Avatar { get; set; }

        /// <summary>
        /// Attachments for this message (Must be SlackAttachment[])
        /// </summary>
        [JsonProperty("attachments")]
        public IAttachment[] Attachments { get; set; }

        /// <summary>
        /// Loads an array of attachments
        /// </summary>
        /// <param name="attach"></param>
        public void LoadAttachments(params IAttachment[] attach) => Attachments = attach;

        /// <summary>
        /// Sends this message to a desired api
        /// </summary>
        public bool Send()
        {
            return false;
        }

        /// <summary>
        /// Sets if Slack will parse names and channels
        /// </summary>
        [JsonProperty("link_names")]
        public bool TagNames { get; set; }

        /// <summary>
        /// Sets if Slack will parse markdown text
        /// </summary>
        [JsonProperty("mrkdwn")]
        public bool Markdown { get; set; }
    }
}