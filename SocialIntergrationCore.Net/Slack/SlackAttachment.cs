﻿using Newtonsoft.Json;
using SocialIntergrationCore.Net.Libraries;

namespace SocialIntergrationCore.Net.Slack
{
    public class SlackAttachment : IAttachment
    {
        /// <summary>
        /// The message that is displayed if an error occurs with format
        /// </summary>
        [JsonProperty("fallback")]
        public string Fallback { get; set; }

        /// <summary>
        /// The message that shows before this attachment
        /// </summary>
        [JsonProperty("pretext")]
        public string Pretext { get; set; }

        /// <summary>
        /// The Title of this attachment
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }

        /// <summary>
        /// Set the link of the title
        /// </summary>
        [JsonProperty("title_link")]
        public string TitleURL { get; set; }

        /// <summary>
        /// Gets or Sets the Attachment body Text
        /// </summary>
        [JsonProperty("text")]
        public string BodyText { get; set; }

        /// <summary>
        /// Set the color of this attachment
        /// </summary>
        [JsonIgnore]
        public Color Color { get; set; }

        /// <summary>
        /// Gets the slack compatible color
        /// </summary>
        [JsonProperty("color")]
        public string _Color { get { return Color.HexString; } }

        /// <summary>
        /// Gets or Sets the AuthorName
        /// </summary>
        [JsonProperty("author_name")]
        public string AuthorName { get; set; }

        /// <summary>
        /// Gets or Sets the Author Link
        /// </summary>
        [JsonProperty("author_link")]
        public string AuthorLink { get; set; }

        /// <summary>
        /// Gets or Sets the Author Icon
        /// </summary>
        [JsonProperty("author_icon")]
        public string AuthorIcon { get; set; }

        /// <summary>
        /// Gets or Sets this attachment image
        /// </summary>
        [JsonProperty("image_url")]
        public string Image { get; set; }

        /// <summary>
        /// Gets or Sets the attachment thumb image
        /// </summary>
        [JsonProperty("thumb_url")]
        public string Thumbnail { get; set; }

        /// <summary>
        /// Gets or Sets the Footer Text
        /// </summary>
        [JsonProperty("footer")]
        public string FooterText { get; set; }

        /// <summary>
        /// Gets or Sets the Footer Icon
        /// </summary>
        [JsonProperty("footer_icon")]
        public string FooterIcon { get; set; }

        /// <summary>
        /// Array of SlackAttachmentFields
        /// </summary>
        [JsonProperty("fields")]
        public IField[] Fields { get; set; }

        /// <summary>
        /// The time to show at the bottom of the attachment footer
        /// </summary>
        [JsonProperty("ts")]
        public long Timestamp { get; set; }

        /// <summary>
        /// Loads the Fields array with provided Fields
        /// </summary>
        /// <param name="fields"></param>
        public void LoadFields(params IField[] fields) => Fields = fields;
    }
}