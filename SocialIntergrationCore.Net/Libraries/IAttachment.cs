﻿namespace SocialIntergrationCore.Net.Libraries
{
    public interface IAttachment
    {
        /// <summary>
        /// Title of this Attachment
        /// </summary>
        string Title { get; set; }

        /// <summary>
        /// Sets the URL of the title
        /// </summary>
        string TitleURL { get; set; }

        /// <summary>
        /// Sets the color identifier
        /// </summary>
        Color Color { get; set; }

        /// <summary>
        /// Author Name
        /// </summary>
        string AuthorName { get; set; }

        /// <summary>
        /// Sets the link for author name
        /// </summary>
        string AuthorLink { get; set; }

        /// <summary>
        /// Sets the Icon for Author
        /// </summary>
        string AuthorIcon { get; set; }

        /// <summary>
        /// The text to appear in this attachments body
        /// </summary>
        string BodyText { get; set; }

        /// <summary>
        /// Sets the Attachment image
        /// </summary>
        string Image { get; set; }

        /// <summary>
        /// Sets the Thumbnail of the Attachment
        /// </summary>
        string Thumbnail { get; set; }

        /// <summary>
        /// Text to Appear in Footer
        /// </summary>
        string FooterText { get; set; }

        /// <summary>
        /// Sets the footer icon
        /// </summary>
        string FooterIcon { get; set; }

        /// <summary>
        /// Fields
        /// </summary>
        IField[] Fields { get; set; }

        /// <summary>
        /// Load the fields for this attachment
        /// </summary>
        void LoadFields(params IField[] fields);
    }
}