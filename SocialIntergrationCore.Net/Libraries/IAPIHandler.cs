﻿using System;
using System.Collections.Generic;

namespace SocialIntergrationCore.Net.Libraries
{
    public interface IAPIHandler
    {
        /// <summary>
        /// The Queue that handles the messages
        /// </summary>
        Queue<IMessage> MessageQueue { get; set; }

        /// <summary>
        /// The Full API Hook
        /// </summary>
        Uri Endpoint { get; set; }

        /// <summary>
        /// Sends a Json Converted messaged to this endpoint
        /// </summary>
        /// <param name="message"></param>
        void Send(IMessage message);

        /// <summary>
        /// Sends an array of messages
        /// </summary>
        /// <param name="messages"></param>
        void Send(IMessage[] messages);
    }
}