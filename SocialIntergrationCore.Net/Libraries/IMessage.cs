﻿namespace SocialIntergrationCore.Net.Libraries
{
    public interface IMessage
    {
        /// <summary>
        /// The Message Body Text
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// The Username the messsage is sent as
        /// </summary>
        string Username { get; set; }

        /// <summary>
        /// The Avatar for this user
        /// </summary>
        string Avatar { get; set; }

        /// <summary>
        /// Attached Content to this message
        /// </summary>
        IAttachment[] Attachments { get; set; }

        /// <summary>
        /// Load Attachments into Attachment array
        /// </summary>
        /// <param name="args">Array of Attachments</param>
        void LoadAttachments(params IAttachment[] args);

        /// <summary>
        /// Attempts to the message
        /// </summary>
        /// <returns>False if Failed | True if Success</returns>
        bool Send();
    }
}