﻿using System;
using System.Threading;

namespace SocialIntergrationCore.Net.Libraries
{
    public class Timer : IDisposable
    {
        /// <summary>
        /// The callback
        /// </summary>
        private Action Callback { get; set; }

        /// <summary>
        /// Notifies the timer that the task is done
        /// </summary>
        private bool Complete
        {
            get { return _Complete; }
            set
            {
                if (value == true)
                {
                    this.Dispose();
                }

                _Complete = value;
            }
        }

        private bool _Complete = false;

        /// <summary>
        /// Thread used to run this timer
        /// </summary>
        private Thread TimerThread { get; set; }

        /// <summary>
        /// Instance a timer that runs once
        /// </summary>
        /// <param name="ms">Time in milliseconds</param>
        /// <param name="Action">Callback action</param>
        public static void Once(int ms, Action Action)
        {
            var timer = new Timer();
            timer.Callback = Action;
            timer.TimerThread = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;

                Thread.Sleep(ms);

                if (timer.Callback != null)
                    timer.Callback?.Invoke();
                timer.Complete = true;
            });

            timer.TimerThread.Start();
        }

        /// <summary>
        /// Instance a timer that runs multiple times
        /// </summary>
        /// <param name="ms">Time in milliseconds</param>
        /// <param name="repeats">The ammount of times your action will be called after every burst</param>
        /// <param name="Action">Callback action</param>
        public static void Repeat(int ms, int repeats, Action Action)
        {
            var timer = new Timer();
            timer.Callback = Action;
            timer.TimerThread = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;

                for (int i = 0; i < repeats; i++)
                {
                    Thread.Sleep(ms);
                    if (timer.Callback != null)
                        timer.Callback?.Invoke();
                }

                timer.Complete = true;
            });

            timer.TimerThread.Start();
        }

        /// <summary>
        /// Disposes this timer
        /// </summary>
        public void Dispose()
        {
            if (TimerThread != null)
            {
                try
                {
                    TimerThread.Abort();
                }
                catch (ThreadAbortException ex)
                {
                    TimerThread = null;
                }
                TimerThread = null;
            }

            Callback = null;
        }
    }
}