﻿using System;
using System.Globalization;

namespace SocialIntergrationCore.Net.Libraries
{
    /// <summary>
    /// Color Enums
    /// </summary>
    public enum Colors
    {
        AQUA,
        GREEN,
        BLUE,
        PURPLE,
        GOLD,
        ORANGE,
        RED,
        GREY,
        DARKER_GREY,
        NAVY,
        DARK_AQUA,
        DARK_GREEN,
        DARK_BLUE,
        DARK_PURPLE,
        DARK_GOLD,
        DARK_ORANGE,
        DARK_RED,
        DARK_GREY,
        LIGHT_GREY,
        DARK_NAVY
    }

    public struct Color
    {
        /// <summary>
        /// Red
        /// </summary>
        public int R { get; internal set; }

        /// <summary>
        /// Green
        /// </summary>
        public int G { get; internal set; }

        /// <summary>
        /// Blue
        /// </summary>
        public int B { get; internal set; }

        /// <summary>
        /// Hex String
        /// </summary>
        public string HexString { get { return "#" + R.ToString("X2") + G.ToString("X2") + B.ToString("X2"); } }

        /// <summary>
        /// Hexadecimal
        /// </summary>
        public int Hex { get { return int.Parse(HexString.Substring(1), NumberStyles.HexNumber); } }

        public static implicit operator Color(string ColorHex) => FromHEX(ColorHex);

        public static implicit operator Color(Colors col)
        {
            if (col == Colors.AQUA)
                return FromArgb(0, 255, 255);
            if (col == Colors.BLUE)
                return FromArgb(0, 0, 128);
            if (col == Colors.DARKER_GREY)
                return FromArgb(73, 73, 73);
            if (col == Colors.DARK_AQUA)
                return FromArgb(0, 150, 150);
            if (col == Colors.DARK_BLUE)
                return FromArgb(0, 0, 135);
            if (col == Colors.DARK_GOLD)
                return FromArgb(184, 185, 0);
            if (col == Colors.DARK_GREEN)
                return FromArgb(0, 100, 0);
            if (col == Colors.DARK_GREY)
                return FromArgb(90, 90, 90);
            if (col == Colors.DARK_NAVY)
                return FromArgb(0, 0, 100);
            if (col == Colors.DARK_ORANGE)
                return FromArgb(165, 100, 0);
            if (col == Colors.DARK_PURPLE)
                return FromArgb(101, 0, 101);
            if (col == Colors.DARK_RED)
                return FromArgb(150, 0, 0);
            if (col == Colors.GOLD)
                return FromArgb(255, 215, 0);
            if (col == Colors.GREEN)
                return FromArgb(0, 128, 0);
            if (col == Colors.GREY)
                return FromArgb(128, 128, 128);
            if (col == Colors.LIGHT_GREY)
                return FromArgb(160, 160, 160);
            if (col == Colors.NAVY)
                return FromArgb(0, 0, 128);
            if (col == Colors.ORANGE)
                return FromArgb(255, 165, 0);
            if (col == Colors.PURPLE)
                return FromArgb(128, 0, 128);
            if (col == Colors.RED)
                return FromArgb(255, 0, 0);

            return null;
        }

        /// <summary>
        /// Converts a Hex to a color class
        /// </summary>
        /// <param name="Hex"></param>
        /// <returns></returns>
        public static Color FromHEX(string Hex)
        {
            if (Hex.StartsWith("#"))
                Hex = Hex.Substring(1);

            if (Hex.Length != 6) throw new Exception("Not a valid color");

            return FromArgb(int.Parse(Hex.Substring(0, 2), NumberStyles.HexNumber), int.Parse(Hex.Substring(2, 2), NumberStyles.HexNumber), int.Parse(Hex.Substring(4, 2), NumberStyles.HexNumber));
        }

        /// <summary>
        /// Converts RGB Values to color class
        /// </summary>
        /// <param name="Red"></param>
        /// <param name="Green"></param>
        /// <param name="Blue"></param>
        /// <returns></returns>
        public static Color FromArgb(int Red, int Green, int Blue)
        {
            var color = new Color();
            color.R = Red;
            color.G = Green;
            color.B = Blue;
            return color;
        }
    }
}