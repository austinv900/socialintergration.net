﻿namespace SocialIntergrationCore.Net.Libraries
{
    public interface IField
    {
        /// <summary>
        /// Title this field
        /// </summary>
        string Title { get; set; }

        /// <summary>
        /// Set the text content of this field
        /// </summary>
        string Value { get; set; }

        /// <summary>
        /// Make the field appear inline (Shorter)
        /// </summary>
        bool Inline { get; set; }
    }
}