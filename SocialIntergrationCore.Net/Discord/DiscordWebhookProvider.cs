﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SocialIntergrationCore.Net.Libraries;

namespace SocialIntergrationCore.Net.Discord
{
    public class DiscordWebhookProvider : IAPIHandler
    {
        /// <summary>
        /// Holds all the pending Discord messages
        /// </summary>
        public Queue<IMessage> MessageQueue { get; set; }

        /// <summary>
        /// The Endpoint used by this provider
        /// </summary>
        public Uri Endpoint { get; set; }

        /// <summary>
        /// Send a message to the discord server
        /// </summary>
        /// <param name="message"></param>
        public void Send(IMessage message)
        {
            /// Post to Discord
        }

        /// <summary>
        /// Send an array of messages to discord
        /// </summary>
        /// <param name="messages"></param>
        public void Send(IMessage[] messages)
        {
            if (MessageQueue == null)
                MessageQueue = new Queue<IMessage>();
            for (int i = 0; i < messages.Count(); i++)
            {
                MessageQueue.Enqueue(messages[i]);
            }

            Timer.Repeat(200, messages.Count(), () =>
            {
                var msg = MessageQueue?.Dequeue();
                if (msg == null)
                    return;
                Send(messages);
            });
        }
    }
}