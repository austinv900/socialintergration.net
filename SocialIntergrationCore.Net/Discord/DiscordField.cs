﻿using Newtonsoft.Json;
using SocialIntergrationCore.Net.Libraries;

namespace SocialIntergrationCore.Net.Discord
{
    /// <summary>
    /// Defines an attachment field
    /// </summary>
    public class DiscordField : IField
    {
        /// <summary>
        /// Gets/Sets the title of this field
        /// </summary>
        [JsonProperty("name")]
        public string Title { get; set; }

        /// <summary>
        /// Get/Set the text for this field
        /// </summary>
        [JsonProperty("value")]
        public string Value { get; set; }

        /// <summary>
        /// Inlines this field
        /// </summary>
        [JsonProperty("inline")]
        public bool Inline { get; set; }
    }
}