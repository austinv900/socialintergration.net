﻿using Newtonsoft.Json;

namespace SocialIntergrationCore.Net.Discord
{
    public class DiscordAuthor
    {
        /// <summary>
        /// The Name of this author
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; internal set; }

        /// <summary>
        /// The url of this author
        /// </summary>
        [JsonProperty("url")]
        public string URL { get; internal set; }
    }
}