﻿using Newtonsoft.Json;
using SocialIntergrationCore.Net.Libraries;

namespace SocialIntergrationCore.Net.Discord
{
    public class DiscordAttachment : IAttachment
    {
        /// <summary>
        /// The Title of this attachment
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string BodyText { get; set; }

        /// <summary>
        /// Give the title a clickable url
        /// </summary>
        [JsonProperty("url")]
        public string TitleURL { get; set; }

        /// <summary>
        /// This value is set by Color
        /// </summary>
        [JsonProperty("color")]
        public int _Color { get { return Color.Hex; } }

        /// <summary>
        /// The author of this Attachment
        /// </summary>
        [JsonProperty("author")]
        public DiscordAuthor Author { get; internal set; }

        /// <summary>
        /// An Array for Fields
        /// </summary>
        [JsonProperty("fields")]
        public IField[] Fields { get; set; }

        /// <summary>
        /// The footer displayed at the bottom of the attachment
        /// </summary>
        [JsonProperty("footer")]
        public DiscordFooter Footer { get; internal set; }

        /// <summary>
        /// Loads Fields into this attachment
        /// </summary>
        /// <param name="fields">DiscordField type Required</param>
        public void LoadFields(params IField[] fields)
        {
            Fields = fields;
        }

        #region Not Supported

        /// <summary>
        /// Not Supported on Discord
        /// </summary>
        [JsonIgnore]
        public string Thumbnail { get; set; }

        /// <summary>
        /// Not Supported on Discord
        /// </summary>
        [JsonIgnore]
        public string Image { get; set; }

        /// <summary>
        /// This is not a compatable field with discord
        /// This will thow an exception
        /// </summary>
        /// <exception cref="NotSupportedException">Always thrown because of not being supported on Discords Native API</exception>
        [JsonIgnore]
        public string AuthorIcon { get; set; }

        /// <summary>
        /// Set the color of this Attachment
        /// </summary>
        [JsonIgnore]
        public Color Color { get; set; }

        /// <summary>
        /// Gets or sets the name of the DiscordAuthor
        /// </summary>
        [JsonIgnore]
        public string AuthorName
        {
            get { return Author?.Name; }
            set
            {
                if (Author == null)
                    Author = new DiscordAuthor();
                Author.Name = value;
            }
        }

        /// <summary>
        /// Gets or Sets the DiscordAuthor Name Url
        /// </summary>
        [JsonIgnore]
        public string AuthorLink
        {
            get { return Author?.URL; }
            set
            {
                if (Author == null)
                    Author = new DiscordAuthor();
                Author.URL = value;
            }
        }

        /// <summary>
        /// Gets or Sets the DiscordFooter Text
        /// </summary>
        [JsonIgnore]
        public string FooterText
        {
            get { return Footer?.Text; }
            set
            {
                if (Footer == null)
                    Footer = new DiscordFooter(value);
                else
                    Footer.Text = value;
            }
        }

        /// <summary>
        /// Gets or Sets the DiscordFooter Icon
        /// </summary>
        [JsonIgnore]
        public string FooterIcon
        {
            get { return Footer?.Icon; }
            set
            {
                if (Footer == null)
                    Footer = new DiscordFooter(null, value);
                else
                    Footer.Icon = value;
            }
        }

        #endregion
    }
}