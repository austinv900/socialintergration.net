﻿using Newtonsoft.Json;

namespace SocialIntergrationCore.Net.Discord
{
    public class DiscordFooter
    {
        /// <summary>
        /// The Text to show on in the footer
        /// </summary>
        [JsonProperty("text")]
        public string Text { get; internal set; }

        /// <summary>
        /// Displays an Icon in the footer
        /// </summary>
        [JsonProperty("icon_url")]
        public string Icon { get; internal set; }

        public DiscordFooter(string FooterBody, string Icon = null)
        {
            Text = FooterBody;
            this.Icon = Icon;
        }
    }
}