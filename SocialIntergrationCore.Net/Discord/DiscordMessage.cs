﻿using Newtonsoft.Json;
using SocialIntergrationCore.Net.Libraries;

namespace SocialIntergrationCore.Net.Discord
{
    /// <summary>
    /// Defines a Discord API Message
    /// </summary>
    public class DiscordMessage : IMessage
    {
        /// <summary>
        /// This message text
        /// </summary>
        [JsonProperty("content")]
        public string Message { get; set; }

        /// <summary>
        /// The this message is sent as
        /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }

        /// <summary>
        /// The avatar icon used for this user
        /// </summary>
        [JsonProperty("avatar_url")]
        public string Avatar { get; set; }

        /// <summary>
        /// Should Discord say this message
        /// </summary>
        [JsonProperty("tts")]
        public bool TextToSpeach { get; set; }

        /// <summary>
        /// Array of Attachments(embeds)
        /// </summary>
        [JsonProperty("embeds")]
        public IAttachment[] Attachments { get; set; }

        /// <summary>
        /// Loads our message with attachments
        /// </summary>
        /// <param name="attach"></param>
        public void LoadAttachments(params IAttachment[] attach)
        {
            Attachments = attach;
        }

        /// <summary>
        /// Sends this message to a specified server
        /// </summary>
        /// <returns></returns>
        public bool Send()
        {
            return false;
        }
    }
}